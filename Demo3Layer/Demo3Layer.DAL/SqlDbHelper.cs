﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Demo3Layer.DAL
{
    public class SqlDbHelper
    {
        private static readonly string ConnectionString = GetConnectionString();

        private static string GetConnectionString()
        {
            Configuration root = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/");
            ConnectionStringSettings con = root.ConnectionStrings.ConnectionStrings["DefaultConnection"];
            return con.ConnectionString;
        }

        public static DataTable ExecuteSelectCommand(string command, CommandType cmdType)
        {
            DataTable table = null;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandType = cmdType;
                        cmd.CommandText = command;

                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            table = new DataTable();
                            da.Fill(table);
                        }
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
                finally
                {
                    con.Close();
                }
            }
            return table;
        }
    }
}
