﻿using System;
using System.Collections.Generic;
using System.Data;
using Demo3Layer.Model;

namespace Demo3Layer.DAL
{
    public class StudentData
    {
        public List<Student> GetAll()
        {
            List<Student> studentList = null;

            using (DataTable table = SqlDbHelper.ExecuteSelectCommand("GetAllStudents", CommandType.StoredProcedure))
            {
                if (table.Rows.Count > 0)
                {
                    studentList = new List<Student>();
                    foreach (DataRow row in table.Rows)
                    {
                        studentList.Add(new Student
                        {
                            Id = Convert.ToInt32(row["Id"]),
                            FirstName = row["FirstName"].ToString(),
                            LastName = row["LastName"].ToString()
                        });

                    }
                }
            }
            return studentList;
        }
    }
}