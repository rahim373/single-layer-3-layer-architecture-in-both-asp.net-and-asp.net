﻿using Demo3Layer.BL;
using System;
using System.Collections.Generic;
using System.Web.UI;
using Demo3Layer.Model;

namespace Demo3Layer.Presentation
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            StudentDataHandler data = new StudentDataHandler();
            List<Student> students = data.GetAllStudent();
            grid.DataSource = students;
            grid.DataBind();
        }
    }
}