﻿using System.Collections.Generic;
using DemoMvc3Layer.DataModel;
using DemoMvc3Layer.DAL;

namespace DemoMvc3Layer.BAL
{
    public class StudentService
    {
        public List<Student> GetAllStudents()
        {
            StudentDataAccessLayer data = new StudentDataAccessLayer();
            return data.GetAllStudent();
        }
    }
}
