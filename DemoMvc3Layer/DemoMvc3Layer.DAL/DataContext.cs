﻿using System.Data.Entity;
using DemoMvc3Layer.DataModel;

namespace DemoMvc3Layer.DAL
{
    public class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {
        }

        public DbSet<Student> Students { get; set; }
    }

}
