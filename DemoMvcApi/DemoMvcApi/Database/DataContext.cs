﻿using System.Data.Entity;
using DemoMvcApi.Models;

namespace DemoMvcApi.Database
{
    public class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {
            
        }

        public DbSet<Student> Students { get; set; }
    }
}