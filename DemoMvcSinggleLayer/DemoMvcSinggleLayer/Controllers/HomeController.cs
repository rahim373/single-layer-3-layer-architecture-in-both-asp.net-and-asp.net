﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DemoMvcSinggleLayer.Database;
using DemoMvcSinggleLayer.Models;

namespace DemoMvcSinggleLayer.Controllers
{
    public class HomeController : Controller
    {
        private readonly DataContext _db;

        public HomeController()
        {
           _db = new  DataContext();
        }

        public ActionResult Index()
        {
            List<Student> students = _db.Students.ToList();
            return View(students);
        }
    }
}