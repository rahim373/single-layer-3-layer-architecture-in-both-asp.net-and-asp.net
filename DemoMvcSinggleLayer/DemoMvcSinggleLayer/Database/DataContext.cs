﻿using System.Data.Entity;
using DemoMvcSinggleLayer.Models;

namespace DemoMvcSinggleLayer.Database
{
    public class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {
            
        }

        public DbSet<Student> Students { get; set; }
    }
}