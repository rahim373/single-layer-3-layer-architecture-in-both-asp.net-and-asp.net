namespace DemoMvcSinggleLayer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DemoMvcSinggleLayer.Database.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DemoMvcSinggleLayer.Database.DataContext context)
        {
             context.Students.AddOrUpdate(
                  p => p.FirstName,
                  new Student { FirstName = "Abdur", LastName = "Rahim"},
                  new Student { FirstName = "Hasan", LastName = "Abdullah"}
                );
        }
    }
}
