﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using DemoSingleLayer.DataAccess;
using DemoSingleLayer.Model;

namespace DemoSingleLayer
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            StudentData data = new StudentData();
            List<Student> students = data.GetAll();
            grid.DataSource = students;
            grid.DataBind();
        }
    }
}